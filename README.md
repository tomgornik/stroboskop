# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://tomgornik@bitbucket.org/tomgornik/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/tomgornik/stroboskop/commits/e0ea501406e2c15a9b082296591f90893efe872d

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/tomgornik/stroboskop/commits/e7ccfdcd091dd7cedc67040c4694f2ef39437310

Naloga 6.3.2:
https://bitbucket.org/tomgornik/stroboskop/commits/24a3c247a54ba10cf17d622794ec2522750cc590

Naloga 6.3.3:
https://bitbucket.org/tomgornik/stroboskop/commits/d8713984212e74941bb10823836fe721ac78b80b

Naloga 6.3.4:
https://bitbucket.org/tomgornik/stroboskop/commits/2a5cb934fa4d848f3502d02f535571e64acd75ec

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/tomgornik/stroboskop/commits/520f5230480a21bd3e28697b5c2ae93711bd1108

Naloga 6.4.2:
https://bitbucket.org/tomgornik/stroboskop/commits/1ffa2ed5f26a0ea9acf4ee751a078464075ee221

Naloga 6.4.3:
https://bitbucket.org/tomgornik/stroboskop/commits/beef120012da46db51238f13eae29d2c93e2ddad

Naloga 6.4.4:
https://bitbucket.org/tomgornik/stroboskop/commits/4d4bb8cbc5937d9e9a571afc9f860470f4742d8f